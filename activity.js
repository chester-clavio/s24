//////////////2///////////////
db.users.find({"firstName":{$regex:/^s/i}},{"firstName":1,"lastName":1,"_id":0})

//////////////3///////////////
db.users.find({$and:[{"department":"HR"},{"age":{$gte:70}}]})

//////////////4///////////////

db.users.find({$and:[{"firstName":{$regex:/^e/i}},{"age":{$lte:30}}]})